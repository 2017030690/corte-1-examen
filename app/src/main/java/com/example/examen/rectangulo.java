package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class rectangulo extends AppCompatActivity {
    private RectanguloActivity RectanguloActivity;
    private TextView lblCliente;
    private TextView lblArea;
    private TextView lblPerimetro;
    private EditText txtBase;
    private EditText txtAltura;
    private Button btnReg;
    private Button btnCal;
    private Button btnLim;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regtangulo);
        lblCliente = (TextView) findViewById(R.id.lblCliente);
        btnReg = (Button) findViewById(R.id.btnSalir);
        btnCal = (Button) findViewById(R.id.btnCalcular);
        btnLim = (Button) findViewById(R.id.btnLimpiar);
        txtBase = (EditText) findViewById(R.id.txtBase);
        txtAltura = (EditText) findViewById(R.id.txtDAltura);
        lblArea = (TextView) findViewById(R.id.lblArea);
        lblPerimetro = (TextView) findViewById(R.id.lblPerimetro);
        RectanguloActivity =  new RectanguloActivity();

        //Sacar el dato String
        Bundle datos = getIntent().getExtras();
        String cliente = datos.getString("cliente");
        lblCliente.setText("Cliente: " + cliente);

        btnCal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtBase.getText().toString().matches("")){
                    Toast.makeText(rectangulo.this,"Debes llenar todos los campos para calcular",Toast.LENGTH_SHORT).show();
                }
                else if(txtAltura.getText().toString().matches("")){
                    Toast.makeText(rectangulo.this,"Debes llenar todos los campos para calcular",Toast.LENGTH_SHORT).show();
                }
                else{
                    String bas = txtBase.getText().toString();
                    float base = Float.parseFloat(bas);
                    String alt = txtAltura.getText().toString();
                    float altura = Float.parseFloat(alt);

                    RectanguloActivity.setBase(base);
                    RectanguloActivity.setAltura(altura);

                    String area = String.valueOf(RectanguloActivity.calcularArea());
                    lblArea.setText("Area: " + area);
                    String perimetro = String.valueOf(RectanguloActivity.calcularPerimetro());
                    lblPerimetro.setText("Perimetro: " + perimetro);
                }
            }
        });

        btnLim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtBase.setText("");
                txtAltura.setText("");
                lblArea.setText("");
                lblPerimetro.setText("");
            }
        });

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
